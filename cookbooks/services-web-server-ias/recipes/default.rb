#
# Cookbook:: services-web-server-ias
# Recipe:: default
#
# Author:: Meghana Kothaluri
#
# Copyright:: 2019, The Authors, All Rights Reserved.



#
# Install Apache Web Server
#
package 'httpd' do
    package_name 'httpd'
    action :upgrade
end

#
# Enable and Start httpd service
#
service 'httpd' do
    action [:enable, :start]
end


#
# Manage httpd config and restart httpd service when changed
#
cookbook_file '/etc/httpd/conf/httpd.conf' do
    source 'httpd/httpd.conf'
    owner 'root'
    group 'root'
    mode '0644'
    notifies(:restart, 'service[httpd]', :delayed)
    action :create
end


#
# Install SSL package for HTTPS 
#
package 'mod_ssl' do
    package_name 'mod_ssl'
    action :upgrade
end

#
# Get the public DNS of active instance running on AWS (for SSL config)
#
metadata_url = URI("http://169.254.169.254/latest/meta-data/public-hostname")
server_name = Net::HTTP.get(metadata_url)


#
# Configure SSL for HTTPS access and restart httpd service
#
template '/etc/httpd/conf.d/ssl.conf' do
  source 'ssl.conf.erb'
  owner 'root'
  group 'root'
  mode '0644'
  variables(server_name: server_name)
  notifies(:restart, 'service[httpd]', :delayed)
  action :create
end


#
# Adding UI files to server
#
remote_directory '/var/www/html' do
    source 'ui'
    owner 'root'
    group 'root'
    mode '0755'
    purge true
    recursive true
    action :create
end


#
# Restart httpd service everyday at 12am UTC
#
cron 'restart_httpd_service' do
    minute '0'
    hour '0'
    command '/bin/systemctl restart httpd.service > /dev/null 2>&1' 
    action :create
end

#
# Rotate httpd logs daily
#
cookbook_file '/etc/logrotate.d/httpd' do
    source 'httpd/httpd_logs'
    owner 'root'
    group 'root'
    mode '0644'
    action :create
end
